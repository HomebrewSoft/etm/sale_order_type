# -*- coding: utf-8 -*-
{
    "name": "Sale Order Type",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev/",
    'license': 'LGPL-3',
    "depends": [
        "mrp",
        "plan_production_order",
        "sale",
    ],
    "data": [
        # security
        "security/ir.model.access.csv",
        # data
        "data/sale_order_type_categories.xml",
        # reports
        # views
        "views/sale_order_type.xml",
        "views/sale_order_line.xml",
        "views/mrp_production.xml",
    ],
}
