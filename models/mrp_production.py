# -*- coding: utf-8 -*-
from odoo import _, api, fields, models, fields
from datetime import datetime, timedelta


class Production(models.Model):
    _inherit = "mrp.production"

    type_order_id = fields.Many2one(
        comodel_name="sale.order.type",
        compute="_compute_type_order_id",
        store=True,
    )
    date_planned_start = fields.Datetime(
        compute="_compute_date_planned_start_new",
    )

    @api.depends("sale_id", "product_id")
    def _compute_type_order_id(self):
        for production in self:
            production.type_order_id = []
            if production.sale_id and production:
                for line in production.sale_id.order_line:
                    if line.product_id == production.product_id:
                        production.type_order_id = line.type_order_id
                        break

    @api.depends("type_order_id")
    def _compute_date_planned_start_new(self):
        for production in self:
            if production.type_order_id:
                production.date_planned_start = fields.Datetime.now() + timedelta(
                    days=production.type_order_id.manufacturing_days
                )
