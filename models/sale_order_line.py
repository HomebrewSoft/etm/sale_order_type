# -*- coding: utf-8 -*-
from odoo import _, api, fields, models, fields


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    type_order_id = fields.Many2one(
        comodel_name="sale.order.type",
        compute="_type_order",
        store=True,
        readonly=False,
    )

    @api.depends("product_id")
    def _type_order(self):
        for line in self:
            production = self.env["mrp.production"].search(
                [("product_id", "=", line.product_id.id), ("state", "=", "done")], limit=1
            )
            if production:
                if line.product_id.is_prototype:
                    line.type_order_id = self.env.ref(
                        "sale_order_type.sale_order_type_restockedwc", raise_if_not_found=False
                    )
                else:
                    line.type_order_id = self.env.ref(
                        "sale_order_type.sale_order_type_restocked", raise_if_not_found=False
                    )
            else:
                line.type_order_id = self.env.ref(
                    "sale_order_type.sale_order_type_new", raise_if_not_found=False
                )
