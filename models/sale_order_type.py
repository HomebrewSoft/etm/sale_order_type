# -*- coding: utf-8 -*-
from odoo import _, api, fields, models, fields


class SaleOrderType(models.Model):
    _name = "sale.order.type"


    name = fields.Char(
        index=True,
        required=True,
    )
    manufacturing_days = fields.Integer(
    )
    delivery_days = fields.Integer(
    )
